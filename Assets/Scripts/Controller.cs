﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class Controller : MonoBehaviour {

	float minCutDistanceSqr;

	float rayLength = 15f;
	int centreLayer;
	int pizzaLayer;

	Camera cam;
	RaycastHit hit;
	Transform hitT;

	int[] trisIndices = new int[2];

	GameObject piece;

	bool canSetFirstPoint=false;
	bool firstPointComplete = false;
	bool canSetSecondPoint=false;
	bool secondPointComplete = false;

	Vector3 firstPoint, secondPoint;
	GameObject pizza;

	int pizzaTrisCount;
	int trisLeft;

	GameObject canvas;
	public GameObject textPrefab;

	void Awake(){
		centreLayer = 1 << LayerMask.NameToLayer ("Pizza center");
		pizzaLayer = 1 << LayerMask.NameToLayer ("Pizza");
		cam = Camera.main;
		
		pizza = GameObject.Find ("Pizza");
		pizzaTrisCount = pizza.GetComponent<MeshFilter> ().sharedMesh.GetIndices (0).Length / 3;
		trisLeft = pizzaTrisCount;

		minCutDistanceSqr = MeshCutter.r * MeshCutter.r * 0.30f;

		canvas = GameObject.Find ("Canvas");
	}



	void Update(){
		if(Input.GetMouseButton(0)){
			if (Physics.Raycast (cam.ScreenPointToRay(Input.mousePosition), out hit, rayLength, centreLayer)) {
				if(!firstPointComplete)
					canSetFirstPoint = true;
				else if (!secondPointComplete)
					canSetSecondPoint = true;
			}

			if (canSetFirstPoint) {
				if (Physics.Raycast (cam.ScreenPointToRay (Input.mousePosition), out hit, rayLength, pizzaLayer)) {
					firstPoint = hit.point;
					if ((hit.point - hit.transform.position).sqrMagnitude > minCutDistanceSqr) {
						trisIndices [0] = hit.triangleIndex;
						firstPointComplete = true;
					}
				}
			}

			if (canSetSecondPoint) {
				if (Physics.Raycast (cam.ScreenPointToRay (Input.mousePosition), out hit, rayLength, pizzaLayer)) {
					secondPoint = hit.point;
					if ((hit.point - hit.transform.position).sqrMagnitude > minCutDistanceSqr) {
						trisIndices [1] = hit.triangleIndex;
						secondPointComplete = true;
					}
				}
			}
		}

		if (Input.GetMouseButtonUp (0)) {
			if (firstPointComplete) {
				canSetFirstPoint = false;
				canSetSecondPoint = true;
			}
			else canSetFirstPoint = false;

			if (secondPointComplete) {
				canSetSecondPoint = false;
				firstPointComplete = false;
				secondPointComplete = false;
				canSetFirstPoint = false;

				int pieceTrisCount;
				MeshCutter.Cut (pizza, new Vector3[]{firstPoint, secondPoint}, trisIndices, out piece, out pieceTrisCount);
				Vector3 dir = pizza.transform.InverseTransformPoint ((firstPoint - secondPoint) / 2+secondPoint).normalized;
				GameObject textObject = Instantiate (textPrefab);
				textObject.transform.SetParent (canvas.transform,true);
				piece.GetComponent<Piece> ().Setup (dir, Random.Range (0.05f, 0.15f),textObject.transform);
				textObject.GetComponent<Text> ().text = "- " + 100 * pieceTrisCount / pizzaTrisCount + " %";

			} else canSetSecondPoint = false;
		}
	}



	#region Debug
	static Vector3 c0, c1, c2, c3, normal;
	public static void PlaneDebug(Plane p, Color c, Vector3 point){
		normal = p.normal;
		Vector3 v3;
		if (p.normal.normalized != Vector3.forward)
			v3 = Vector3.Cross (normal, Vector3.forward).normalized * normal.magnitude;
		else
			v3 = Vector3.Cross (normal, Vector3.up).normalized * normal.magnitude;

		c0 = point + v3;
		c2 = point - v3;
		Quaternion q = Quaternion.AngleAxis (90, normal);
		v3 = q * v3;
		c1 = point + v3;
		c3 = point - v3;

		Debug.DrawLine (c0, c2, c);
		Debug.DrawLine (c1, c3, c);
		Debug.DrawLine (c0, c1, c);
		Debug.DrawLine (c1, c2, c);
		Debug.DrawLine (c2, c3, c);
		Debug.DrawLine (c3, c0, c);
		Debug.DrawRay (point, normal * 0.4f, Color.white);
	}

	void OnDrawGizmos(){
		if (canSetFirstPoint||firstPointComplete) {
			Gizmos.color = Color.blue;
			Gizmos.DrawWireSphere (firstPoint, 0.02f);
		}

		if (canSetSecondPoint||secondPointComplete) {
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere (secondPoint, 0.02f);
		}
	}

	#endregion
}
