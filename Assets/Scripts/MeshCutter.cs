﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshCutter {


	public static float r = 0.5f; // радиус

	static Material mat;
	static Transform cutT;

	static Mesh cutMesh;
	static Vector3[] _vertices;
	static Vector2[] _uvs;
	static Vector3[] _normals;
	static int[] _indices;

	static Plane left;
	static Plane right;
	static int[] cutTris;
	static Vector3[] cutPoints;

	static int cVertex;

	public static void Cut(GameObject go, Vector3[] points, int[] trisIndices, out GameObject piece, out int pieceTrisCount){
		mat = go.GetComponent<Renderer> ().sharedMaterial;
		cutT = go.transform;
		cutPoints = points;
		cutTris = trisIndices;

		cutMesh = go.GetComponent<MeshFilter>().mesh;
		_vertices = cutMesh.vertices;
		_uvs = cutMesh.uv;
		_normals = cutMesh.normals;
		_indices = cutMesh.triangles;

		cVertex = GetCentreVertexID ();

		MeshData minor = new MeshData ();
		MeshData major = new MeshData ();

		if (points [0] != Vector3.zero && points [1] != Vector3.zero) {
			if (Vector3.Cross (cutT.InverseTransformPoint(points [0]), cutT.InverseTransformPoint(points [1])).z > 0) {
				left = new Plane (points [0], cutT.position, cutT.position + Vector3.forward);
				right = new Plane (points [1],  cutT.position + Vector3.forward, cutT.position);
			} else {
				left = new Plane (points [1], cutT.position, cutT.position + Vector3.forward);
				right = new Plane (points [0], cutT.position + Vector3.forward, cutT.position);
			}
		}

		//Controller.PlaneDebug (left, Color.blue, cutT.position);
		//Controller.PlaneDebug (right, Color.red, cutT.position);
		//Debug.Break ();

		int trisCount = _indices.Length/3;
		for (int i = 0; i < trisCount; i++) {
			if (AddToMinorPart (i)) {
				minor.AddTriangle (
					new Vector3[]{ _vertices [_indices [i * 3]], 	_vertices [_indices [i * 3 + 1]],	 _vertices [_indices [i * 3 + 2]] },
					new Vector2[]{ _uvs [_indices [i * 3]],		_uvs [_indices [i * 3 + 1]],		 _uvs [_indices [i * 3 + 2]] },
					new Vector3[]{ _normals [_indices [i * 3]],	_normals [_indices [i * 3 + 1]],	 _normals [_indices [i * 3 + 2]] },
					new int[]{_indices [i * 3],					_indices [i * 3 + 1],			 _indices [i * 3 + 2] }
				);
			} else {
				major.AddTriangle (
					new Vector3[]{ _vertices [_indices [i * 3]], 	_vertices [_indices [i * 3 + 1]],	 _vertices [_indices [i * 3 + 2]] },
					new Vector2[]{ _uvs [_indices [i * 3]],		_uvs [_indices [i * 3 + 1]],		 _uvs [_indices [i * 3 + 2]] },
					new Vector3[]{ _normals [_indices [i * 3]],	_normals [_indices [i * 3 + 1]],	 _normals [_indices [i * 3 + 2]] },
					new int[]{ _indices [i * 3],					_indices [i * 3 + 1],			 _indices [i * 3 + 2] }
				);
			}
		}


		Mesh minorMesh = minor.Generate ();
		GameObject minorObject = new GameObject ("part of pizza", typeof(MeshFilter), typeof(MeshRenderer), typeof(Piece));
		minorObject.transform.position = cutT.position;
		minorObject.transform.rotation = cutT.rotation;
		minorObject.GetComponent<MeshFilter> ().mesh = minorMesh;
		minorObject.GetComponent<MeshRenderer> ().material = mat;

		Mesh majorMesh = major.Generate ();
		go.GetComponent<MeshFilter> ().mesh = majorMesh;
		go.GetComponent<MeshCollider> ().sharedMesh = majorMesh;


		piece = minorObject;
		pieceTrisCount = minorMesh.triangles.Length / 3;
	}


	static bool AddToMinorPart(int trisIndex){
		List<int> vList = new List<int>();
		// убирается "центральный" вертекс из треугольника
		for (int i = 0; i < 3; i++) {
			if (_indices [trisIndex * 3 + i] != cVertex) {
				vList.Add (_indices [trisIndex * 3 + i]);
			}
		}


		bool[] sides = new bool[4];
		sides [0] = left.GetSide (cutT.TransformPoint (_vertices[vList [0]]));
		sides [1] = left.GetSide (cutT.TransformPoint (_vertices[vList [1]]));
		sides [2] = right.GetSide (cutT.TransformPoint (_vertices[vList [0]]));
		sides [3] = right.GetSide (cutT.TransformPoint (_vertices[vList [1]]));
		
		float dist;
		if (trisIndex == cutTris [0]) {
			if (sides [0]) { // 0 вертекс внутри маленького куска
				left.Raycast (new Ray (cutT.TransformPoint (_vertices[vList [0]]), (_vertices [vList [1]] - _vertices [vList [0]]).normalized), out dist);
				if (dist / (_vertices [vList [1]] - _vertices [vList [0]]).magnitude > 0.5f)
					return true;
				else
					return false;
			} else {
				left.Raycast (new Ray (cutT.TransformPoint (_vertices[vList [1]]), (_vertices [vList [0]] - _vertices [vList [1]]).normalized), out dist);
				if (dist / (_vertices [vList [0]] - _vertices [vList [1]]).magnitude > 0.5f)
					return true;
				else
					return false;
			}
		} else if (trisIndex == cutTris [1]) {
			if (sides [2]) { // 2 вертекс внутри маленького куска
				right.Raycast (new Ray (cutT.TransformPoint (_vertices[vList [0]]), (_vertices [vList [1]] - _vertices [vList [0]]).normalized), out dist);
				if (dist / (_vertices [vList [1]] - _vertices [vList [0]]).magnitude > 0.5f)
					return true;
				else
					return false;
			} else {
				right.Raycast (new Ray (cutT.TransformPoint (_vertices[vList [1]]), (_vertices [vList [0]] - _vertices [vList [1]]).normalized), out dist);
				if (dist / (_vertices [vList [0]] - _vertices [vList [1]]).magnitude > 0.5f)
					return true;
				else
					return false;
			}
		}


		return sides [0] && sides [1] && sides [2] && sides [3];
	}

	static float ArcLength(float theta){
		return r * theta;
	}


	static int GetCentreVertexID(){
		Vector3 v0 = _vertices [_indices [0]];
		Vector3 v1 = _vertices [_indices [1]];
		Vector3 v2 = _vertices [_indices [2]];

		float d0 = (v0 - Vector3.zero).sqrMagnitude;
		float d1 = (v1 - Vector3.zero).sqrMagnitude;
		float d2 = (v2 - Vector3.zero).sqrMagnitude;

		if (d0 < d1) {
			if (d0 < d2) return _indices [0];
			else return _indices[2];
		} else {
			if (d1 < d2) return _indices [1];
			else return _indices[2];
		}
	}




	class MeshData {
		List<Vector3> v = new List<Vector3>(); // вершины
		List<Vector2> u= new List<Vector2>(); // текстурные коорды
		List<Vector3> n= new List<Vector3>(); // нормали
		List<int> t= new List<int>();	 // трисы
		Dictionary<int,int> ind = new Dictionary<int, int>(); // связующий словарь < "индекс вершины в целом меше" -- "индекс вершины в этом меше" >

		public void AddTriangle(Vector3[] vertices, Vector2[] uvs, Vector3[] normals, int[] indices){
			
			if (!ind.ContainsKey (indices [0])) {
				
				ind.Add (indices [0], v.Count);

				v.Add (vertices [0]);
				u.Add (uvs [0]);
				n.Add (uvs [0]);
			}
			if (!ind.ContainsKey (indices [1])) {
				ind.Add (indices [1], v.Count);

				v.Add (vertices [1]);
				u.Add (uvs [1]);
				n.Add (uvs [1]);
			}
			if (!ind.ContainsKey (indices [2])) {
				ind.Add (indices [2], v.Count);

				v.Add (vertices [2]);
				u.Add (uvs [2]);
				n.Add (uvs [2]);
			}

			t.Add (ind [indices [0]]);
			t.Add (ind [indices [1]]);
			t.Add (ind [indices [2]]);
		}

		public Mesh Generate(){
			Mesh mesh = new Mesh ();
			mesh.name = "Generated Mesh";
			mesh.SetVertices (v);
			mesh.SetUVs (0, u);
			mesh.SetNormals (n);
			mesh.SetTriangles (t, 0);

			return mesh;
		}


	}
}
