﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour {

	Vector3 dir;
	float speed;
	static float speedAccel = 1.07f;
	Transform text;

	float step = 0f;
	public void Setup(Vector3 dir, float speed, Transform text){
		this.dir = dir;
		this.speed = speed;
		this.text = text;
	}

	void Update(){
		step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards (transform.position, dir*10f, step);
		text.position = transform.position + (dir * MeshCutter.r);
		speed *= speedAccel;
	}
}
